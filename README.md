# Thorium Reader unofficial snap package

Thorium Reader is an easy to use EPUB reading application for cross platforms.

* https://github.com/edrlab/thorium-reader

This is unofficial re-packing of the debian package provided by EDRLab.

## How to build the snap package on local system

First of all, you needs to install snapcraft: https://snapcraft.io/docs/build-on-lxd

```
$ sudo snap install snapcraft --classic

Install LXD if you are on Ubuntu Desktop:
$ sudo snap install lxd

Initialize LXD enveronment:
$ sudo lxd init

Re-login to environment to join lxd grou:
```

Checkout this repository.

```
$ git clone https://gitlab.com/mtyshibata/thorium-reader-snap.git
$ cd thorium-reader-snap
```

Build the snap package:

```
$ snapcraft --use-lxd
```

Install this package:

```
$ sudo snap install ./thorium-reader-snap_2.0.0_amd64.snap  --dangerous
```

## How to build the snap on Launchpad

```
$ snapcraft remote-build
```

## References

* https://gihyo.jp/admin/serial/01/ubuntu-recipe/0654
